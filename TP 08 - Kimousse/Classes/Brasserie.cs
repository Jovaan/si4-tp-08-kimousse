﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Brasserie
    {
        public int Id { get; set; }
        public string Nom { get; }
        public string Ville { get; }
        public int IdRegion { get; set; }
    }
}
