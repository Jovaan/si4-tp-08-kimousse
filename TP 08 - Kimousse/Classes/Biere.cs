﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Biere
    {
        public int IdMarque { get; set; }
        public string Version { get; set; }
        public int IdType { get; set; }
        public string CouleurBiere { get; set; }
        public float TauxAlcool { get; set; }
        public string Caracteristiques { get; set; }
    }
}
